from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType


def enmascararColumnas(data):
    return "Este dato esta enmascarado"
    
enmascararUDF=udf(enmascararColumnas,StringType()) 


initialFile='gs://repositorio-inicial-data-temperatura/GlobalLandTemperaturesByCountry.csv'
df =spark.read.csv(initialFile,inferSchema=True, header =True)
df.show()
df = df.withColumn("dt", enmascararUDF(df.dt))
df.show(2)
df1= df.select('Country').distinct()
countryArray = df1.rdd.flatMap(lambda x: x).collect()




for element in countryArray:    
    countrydf=df.filter(df.Country==element) 
    countrydf.write.csv('gs://repositorio-final-data-temperatura/'+ element+'.csv')
