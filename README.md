# PROCESAR ARCHIVO AUTOMATICAMENTE

Arquitectura

Una Cloud Function vigila el almacenamiento de un recurso en un Storage de GCP. En cuanto llega el archivo la función se ejecuta y dispara un código que va a ejecutar un job en un cluster de Spark (DataProc). 

Ese código en Pyspark esta encargado de enmascarar los datos de la columna "df" y subdividir el archivo por un filtro según el nombre del país para guardarlo en un bucket de salida.

El respositorio de entrada que vigila la Cloud Function es:gs://repositorio-inicial-data-temperatura
Y el repositorio de salida de los archivos, donde se encuentran subdivididos es: gs://repositorio-final-data-temperatura
